#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char Byte;
typedef unsigned int Word;

#define DIGEST_SIZE 32
#define BLOCK_SIZE 64

static inline Word shiftRight(Word x, Word n)
{
	return (x >> n);
}

static inline Word rotateRight(Word x, Word n)
{
	return ((x >> n) | (x << ((sizeof x << 3) - n)));
}

static inline Word choose(Word x, Word y, Word z)
{
	return ((x & y) ^ (~x & z));
}

static inline Word getMajority(Word x, Word y, Word z)
{
	return ((x & y) ^ (x & z) ^ (y & z));
}

static inline Word getSigmaUpper0(Word x)
{
	return (rotateRight(x, 2) ^ rotateRight(x, 13) ^ rotateRight(x, 22));
}

static inline Word getSigmaUpper1(Word x)
{
	return (rotateRight(x, 6) ^ rotateRight(x, 11) ^ rotateRight(x, 25));
}

static inline Word getSigmaLower0(Word x)
{
	return (rotateRight(x, 7) ^ rotateRight(x, 18) ^ shiftRight(x, 3));
}

static inline Word getSigmaLower1(Word x)
{
	return (rotateRight(x, 17) ^ rotateRight(x, 19) ^ shiftRight(x, 10));
}

const Word K[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
	0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
	0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
	0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
	0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
	0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
	0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
	0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
	0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

static void computeHash(Word *hash, Word *messageBlock)
{
	Word messageSchedule[64];
	Word a, b, c, d, e, f, g, h;
	Word temp1, temp2;
	for (Byte i = 0; i != 16; i++) {
		messageSchedule[i] =
			(messageBlock[4 * i] << 24) |
			(messageBlock[4 * i + 1] << 16) |
			(messageBlock[4 * i + 2] << 8) |
			(messageBlock[4 * i + 3]);
	}
	for (Byte i = 16; i != 64; i++) {
		messageSchedule[i] =
			getSigmaLower1(messageSchedule[i - 2]) + messageSchedule[i - 7] +
			getSigmaLower0(messageSchedule[i - 15]) + messageSchedule[i - 16];
	}
	a = hash[0];
	b = hash[1];
	c = hash[2];
	d = hash[3];
	e = hash[4];
	f = hash[5];
	g = hash[6];
	h = hash[7];
	for (Byte i = 0; i != 64; i++) {
		temp1 = h + getSigmaUpper1(e) + choose(e, f, g) + K[i] + messageSchedule[i];
		temp2 = getSigmaUpper0(a) + getMajority(a, b, c);
		h = g;
		g = f;
		f = e;
		e = d + temp1;
		d = c;
		c = b;
		b = a;
		a = temp1 + temp2;
	}
	hash[0] = hash[0] + a;
	hash[1] = hash[1] + b;
	hash[2] = hash[2] + c;
	hash[3] = hash[3] + d;
	hash[4] = hash[4] + e;
	hash[5] = hash[5] + f;
	hash[6] = hash[6] + g;
	hash[7] = hash[7] + h;
}

Byte* sha256(const char* message)
{
	Byte* digest = (Byte*)malloc(DIGEST_SIZE * sizeof (Byte));
	if (!digest) {
		return NULL;
	}
	size_t messageLength = strlen(message);
	Word hash[8] = {
		0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
		0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
	};
	Word messageBlock[BLOCK_SIZE];
	Word blockSize = 0;
	for (size_t i = 0; i != messageLength; i++) {
		messageBlock[blockSize] = message[i];
		blockSize = blockSize + 1;
		if (blockSize == BLOCK_SIZE) {
			computeHash(hash, messageBlock);
			blockSize = 0;
		}
	}
	messageBlock[blockSize] = 0x80;
	blockSize = blockSize + 1;
	if (blockSize < 56) {
		while(blockSize != 56) {
			messageBlock[blockSize] = 0x00;
			blockSize = blockSize + 1;
		}
	} else {
		while(blockSize != 64) {
			messageBlock[blockSize] = 0x00;
			blockSize = blockSize + 1;
		}
		computeHash(hash, messageBlock);
		memset(messageBlock, 0, 56);
	}
	messageLength = messageLength * 8;
	messageBlock[56] = messageLength >> 56;
	messageBlock[57] = messageLength >> 48;
	messageBlock[58] = messageLength >> 40;
	messageBlock[59] = messageLength >> 32;
	messageBlock[60] = messageLength >> 24;
	messageBlock[61] = messageLength >> 16;
	messageBlock[62] = messageLength >> 8;
	messageBlock[63] = messageLength;
	computeHash(hash, messageBlock);
	for (Word i = 0; i != 8; i++) {
		digest[4 * i] = hash[i] >> 24;
		digest[4 * i + 1] = hash[i] >> 16;
		digest[4 * i + 2] = hash[i] >> 8;
		digest[4 * i + 3] = hash[i];
	}
	return digest;
}
