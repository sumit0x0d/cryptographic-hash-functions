#include <stdio.h>
#include <string.h>

typedef unsigned char Byte;
typedef unsigned long Word;

#define DIGEST_SIZE 32
#define BLOCK_SIZE 128

static inline Word shiftRight(Word x, Word n)
{
	return (x >> n);
}

static inline Word rotateRight(Word x, Word n)
{
	return ((x >> n) | (x << ((sizeof x << 3) - n)));
}

static inline Word choose(Word x, Word y, Word z)
{
	return ((x & y) ^ (~x & z));
}

static inline Word getMajority(Word x, Word y, Word z)
{
	return ((x & y) ^ (x & z) ^ (y & z));
}

static inline Word getSigmaUpper0(Word x)
{
	return (rotateRight(x, 28) ^ rotateRight(x, 34) ^ rotateRight(x, 39));
}

static inline Word getSigmaUpper1(Word x)
{
	return (rotateRight(x, 14) ^ rotateRight(x, 18) ^ rotateRight(x, 41));
}

static inline Word getSigmaLower0(Word x)
{
	return (rotateRight(x, 1) ^ rotateRight(x, 8) ^ shiftRight(x, 7));
}

static inline Word getSigmaLower1(Word x)
{
	return (rotateRight(x, 19) ^ rotateRight(x, 61) ^ shiftRight(x, 6));
}

static const Word K[80] = {
	0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
	0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
	0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
	0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
	0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
	0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
	0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
	0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
	0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
	0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
	0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
	0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
	0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
	0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
	0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
	0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
	0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
	0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
	0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
	0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
};

void computeHash(Word *hash, Word *messageBlock)
{
	Word messageSchedule[80];
	Word a, b, c, d, e, f, g, h;
	Word temp1, temp2;
	for (Word t = 0; t != 16; ++t)
		messageSchedule[t] =
			(messageBlock[8 * t] << 56) |
			(messageBlock[8 * t + 1] << 48) |
			(messageBlock[8 * t + 2] << 40) |
			(messageBlock[8 * t + 3] << 32) |
			(messageBlock[8 * t + 4] << 24) |
			(messageBlock[8 * t + 5] << 16) |
			(messageBlock[8 * t + 6] << 8) |
			(messageBlock[8 * t + 7]);
	for (Word t = 16 ; t != 80; ++t)
		messageSchedule[t] =
			getSigmaLower1(messageSchedule[t - 2]) + messageSchedule[t - 7] +
			getSigmaLower0(messageSchedule[t - 15]) + messageSchedule[t - 16];
	a = hash[0];
	b = hash[1];
	c = hash[2];
	d = hash[3];
	e = hash[4];
	f = hash[5];
	g = hash[6];
	h = hash[7];
	for (Word t = 0; t != 80; ++t) {
		temp1 = h + getSigmaUpper1(e) + choose(e, f, g) + K[t] + messageSchedule[t];
		temp2 = getSigmaUpper0(a) + getMajority(a, b, c);
		h = g;
		g = f;
		f = e;
		e = d + temp1;
		d = c;
		c = b;
		b = a;
		a = temp1 + temp2;
	}
	hash[0] += a;
	hash[1] += b;
	hash[2] += c;
	hash[3] += d;
	hash[4] += e;
	hash[5] += f;
	hash[6] += g;
	hash[7] += h;
}

Byte* sha512(const char *message)
{
	Word hash[8] = {
		0x6a09e667f3bcc908, 0xbb67ae8584caa73b, 0x3c6ef372fe94f82b, 0xa54ff53a5f1d36f1,
		0x510e527fade682d1, 0x9b05688c2b3e6c1f, 0x1f83d9abfb41bd6b, 0x5be0cd19137e2179
	};
	Word messageBlock[BLOCK_SIZE];
	Word blockSize = 0;
	Word l = 0;
	static Byte digest[64];
	while (message[l] != '\0') {
		++l;
	}
	for (Word i = 0; i != l; ++i) {
		messageBlock[blockSize] = message[i];
		++blockSize;
		if (blockSize == 128) {
			computeHash(hash, messageBlock);
			blockSize = 0;
		}
	}
	messageBlock[blockSize] = 0x80;
	if (blockSize < 112) {
		while (blockSize != 111)
			messageBlock[++blockSize] = 0x00;
	} else {
		while (blockSize != 127)
			messageBlock[++blockSize] = 0x00;
		computeHash(hash, messageBlock);
		for (Word i = 0; i != 112; ++i)
			messageBlock[i] = 0;
	}
	l *= 8;
	messageBlock[112] = 0;//l >> 120;
	messageBlock[113] = 0;//l >> 112;
	messageBlock[114] = 0;//l >> 104;
	messageBlock[115] = 0;//l >> 96;
	messageBlock[116] = 0;//l >> 88;
	messageBlock[117] = 0;//l >> 80;
	messageBlock[118] = 0;//l >> 72;
	messageBlock[119] = 0;//l >> 64;
	messageBlock[120] = l >> 56;
	messageBlock[121] = l >> 48;
	messageBlock[122] = l >> 40;
	messageBlock[123] = l >> 32;
	messageBlock[124] = l >> 24;
	messageBlock[125] = l >> 16;
	messageBlock[126] = l >> 8;
	messageBlock[127] = l;
	computeHash(hash, messageBlock);
	for (Word i = 0; i < 8; ++i) {
		digest[8*i] = hash[i] >> 56;
		digest[8*i+1] = hash[i] >> 48;
		digest[8*i+2] = hash[i] >> 40;
		digest[8*i+3] = hash[i] >> 32;
		digest[8*i+4] = hash[i] >> 24;
		digest[8*i+5] = hash[i] >> 16;
		digest[8*i+6] = hash[i] >> 8;
		digest[8*i+7] = hash[i];
	}
	return digest;
}
